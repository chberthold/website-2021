function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}



var form = document.getElementById("fo_log");
form.addEventListener("submit", event =>{
    var xmlhttp
    if (window.XMLHttpRequest)
    { 
	xmlhttp = new XMLHttpRequest();
    } 
    else  if (window.ActiveXObject)
    {
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "/htbin/login.py";
    var params = "username=" + document.getElementById("username").value + "&userpwd=" + document.getElementById("userpwd").value;
    xmlhttp.onreadystatechange=function()
    {
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
	    document.getElementById("message").innerHTML= '<p>'+ xmlhttp.responseText +'</p><button class="red-x" onclick="closemess()">X</button>'; 
	}
    }
    xmlhttp.open("GET",url + "?" + params, true);
    xmlhttp.load = function(){
	if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
	    document.getElementById("message").innerHTML = xmlhttp.responseText;
	}
    }
    xmlhttp.send(null);
    closeForm();
    event.preventDefault();
});


function closemess(){
    document.getElementById('message').innerHTML = "";
}
