var form  = document.getElementsByTagName('form')[0];
var email = document.getElementById('useremail');
var pwd = document.getElementById('userpwd');
var pseudo = document.getElementById('username');
var nom = document.getElementById('lastname');
var prenom = document.getElementById('firstname');
var errorm = document.getElementById('erroruseremail');
var errorpwd = document.getElementById('errorpwd');
var emailRegExp = /\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/;
var pwdRegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;



prenom.addEventListener('keyup', function () {
    var test = (prenom.value.length !== 0);
    if(!test){
	prenom.className = "invalid";
    }
    else{
	prenom.className = "valid";
    }
    return test;
});



nom.addEventListener('keyup', function () {
    var test = (nom.value.length !== 0);
    if(!test){
	nom.className = "invalid";
    }
    else{
	nom.className = "valid";
    }
    return test;
});


pseudo.addEventListener('keyup', function () {
    var test = (pseudo.value.length > 5);
    if(!test){
	pseudo.className = "invalid";
    }
    else{
	pseudo.className = "valid";
    }
    return test;
});



email.addEventListener('keyup', function () {
    var test = email.value.length !== 0 && emailRegExp.test(email.value);
    
    if (!test) {
	email.className = "invalid";
	errorm.innerHTML = "\nMerci d'écrire une adresse e-mail valide.";
	errorm.className = "erroractive";
	
	return false;
    } else {
	email.className = "valid";
	errorm.innerHTML = "";
	errorm.className = "error";
    }
});


/* POUR MDP : */



pwd.addEventListener('keyup', function () {
    var test = pwd.value.length > 7 && pwdRegExp.test(pwd.value);
    if (!test) {
	pwd.className = "invalid";
	errorpwd.innerHTML = "Merci d'écrire un mot de passe valide.";
	errorpwd.className = "erroractive";

	return false;
    } else {
	pwd.className = "valid";
	errorpwd.innerHTML = "";
	errorpwd.className = "error";
    }
});



/* POUR LA DATE DE NAISSANCE */

var chDate = document.getElementById('birthdate');
var errorbirth = document.getElementById('errordate');




chDate.testDate = function (){
    var objDate,
        mSeconds, 
	day,
	month,
	year;
    if (this.value.substring(2, 3) !== '/' || this.value.substring(5, 6) !== '/') { 
        return false; 
    } 

    month = this.value.substring(3, 5) - 1;
    day = this.value.substring(0, 2) - 0; 
    year = this.value.substring(6, 10) - 0;

    mSeconds = (new Date(year, month, day)).getTime();
    
    objDate = new Date(); 
    objDate.setTime(mSeconds); 

    if (objDate.getFullYear() !== year || 
        objDate.getMonth() !== month || 
        objDate.getDate() !== day) { 
        return false; 
    } 
    
    var res;
    var dateActu = new Date();
    if  (this.value.length == 10 && year < dateActu.getFullYear())
    {
	res = true;
    }
    else {
	res = false;
    }
    return res;
};


chDate.addEventListener('keyup', function () {
    var test = chDate.value.length === 0 || chDate.testDate();
    
    if (!test) {
	chDate.className = "invalid";
	errorbirth.innerHTML = "Merci d'écrire une date valide.";
	errorbirth.className = "erroractive";
	
    } else {
	chDate.className = "valid";
	errorbirth.innerHTML = "";
	errorbirth.className = "error";
    }
});






var sub = document.getElementById('sub');



window.addEventListener('keyup', function(){

    if(email.className == "valid" && pwd.className == "valid" && chDate.className == "valid" && prenom.className == "valid" && nom.className == "valid" && pseudo.className == "valid"){
	sub.disabled = false;
    }
    else{
	sub.disabled = true;
    }
});
