var form = document.getElementById("mess");
form.addEventListener("submit", event =>{
    
    $.ajax({
        url : "/htbin/chatsend.py",
        type : "POST",
        data : "msg=" + $("#usermsg").val(),
	dataType : 'json',
	success : function(resultat){
	    var str = resultat.msg;
	    document.getElementById("error").innerHTML = str;
	}
    });
    

    event.preventDefault();
    
});



function charger(){

    setTimeout( function(){
        $.ajax({
            url : "/htbin/chatget.py",
            type : "GET",
            success : function(json){
		document.getElementById("chatbox").innerHTML = "";
		var mess = ''
		for(var i in json){
		    mess = json[i].date + " " + json[i].time + " " + json[i].user + " : " + json[i].msg;
		    $("#chatbox").prepend('<p>'+mess+'</p>');
		}
            }
        });

        charger();

    }, 3000);

}

charger();
